package com.model;

/**
 * Created by craterzone on 6/11/15.
 */
public class BookMarkModel {
    public BookMarkModel(long pageNumber) {
        this.pageNumber = pageNumber;
    }

    public long getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(long pageNumber) {
        this.pageNumber = pageNumber;
    }

    private long pageNumber;


}
