package com.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.model.BookMarkModel;

import java.util.ArrayList;

public class Dao {

    public static final String TAG = Dao.class.getName();
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper = null;
    private static Dao _instance;
    private Context _context;

    private Dao(Context context) {
        if (dbHelper == null) {
            this._context = context;

            dbHelper = SQLiteHelper.getInstance(context);
        }
    }

    public static Dao getInstance(Context context) {
        if (_instance == null) {
            _instance = new Dao(context);
        }
        return _instance;
    }

    public boolean isOpen() {
        if (database != null) {
            return database.isOpen();
        }
        return false;
    }

    public void open() throws SQLException {
        if (dbHelper == null) {
            dbHelper = SQLiteHelper.getInstance(_context);
        } else {
            database = dbHelper.getWritableDatabase();
        }
    }

    public void close() {
        if (dbHelper != null) {
            dbHelper.close();
        }
    }

    /**
     * Open DB if Closed
     */
    public void openDBIfClosed() {
        if (!isOpen()) {
            open();
        }
    }

    /**
     * Insert user in DB if already exist then replace user
     *
     * @param
     */


    public void insertBookMark(BookMarkModel data) {

        openDBIfClosed();
        try {
            if (database != null) {
                String sql = "INSERT INTO " + Tables.BOOKMARKDATA.getLabel()
                        + " VALUES (?);";
                SQLiteStatement statement = database.compileStatement(sql);
                database.beginTransaction();
                statement.bindLong(1, data.getPageNumber());
                statement.execute();
                database.setTransactionSuccessful();
                database.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Enable to Add Patient in Database :", e);
        }
    }

    /**
     * Get member List From DB
     *
     * @return member List and if error or not found then null
     */

    public void deleteBookMark(int pageNumber) {
        openDBIfClosed();
        try {
            if (database != null) {
                String sql = "DELETE FROM " + Tables.BOOKMARKDATA.getLabel()
                        + " WHERE " + Tables.BookMarkTable.COL_PAGE_NUMBER.getLabel() + "=" + pageNumber;
                SQLiteStatement statement = database.compileStatement(sql);
                database.beginTransaction();
                statement.execute();
                database.setTransactionSuccessful();
                database.endTransaction();
            }
        } catch (Exception e) {
            Log.e(TAG, "Enable to Add Patient in Database :", e);
        }
    }

    public ArrayList<BookMarkModel> getBookMarks() {
        /**
         * Check if DB is open : if not open then open
         */
        openDBIfClosed();
        Cursor cursor = null;
        ArrayList<BookMarkModel> planlist = new ArrayList<BookMarkModel>();

        try {
            if (database != null) {
                String query = "SELECT * FROM "
                        + Tables.BOOKMARKDATA.getLabel();
                cursor = database.rawQuery(query, null);
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        BookMarkModel data = new BookMarkModel(cursor.getLong(0));
                        planlist.add(data);
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Unable to get data from database", e);
        } finally {
            SQLiteHelper.close(cursor);
        }
        return planlist;
    }

}
