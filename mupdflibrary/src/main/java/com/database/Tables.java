package com.database;

public enum Tables {
    BOOKMARKDATA("bookmark_data");
    private String label;

    private Tables(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public enum BookMarkTable {

        COL_PAGE_NUMBER("page_number");
        private String label;

        private BookMarkTable(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }

        public static String createTableQuery() {
            return "CREATE TABLE IF NOT EXISTS "
                    + Tables.BOOKMARKDATA.getLabel() + "("
                    + COL_PAGE_NUMBER.getLabel() + " text "
                    + ")";
        }
    }

}
