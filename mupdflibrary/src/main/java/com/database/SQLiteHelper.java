package com.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This Class handle All thind Related to Database : 1. Create DB, Upgrade,
 * Create tablbe , Upgrade table, Open and Close Db
 */
public class SQLiteHelper extends SQLiteOpenHelper {
    public static final String TAG = SQLiteHelper.class.getName();

    public static final int DATABASE_VERSION = 1;
    /* can't change this name */
    public static final String DATABASE_NAME = "books.db";
    private static SQLiteHelper _instance = null;
    private int openedConnections = 0;

    private SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Getting instance of Sqlite class
     *
     * @param context
     * @return instance Sqlite Helper
     */
    public static SQLiteHelper getInstance(Context context) {
        if (_instance == null) {
            synchronized (SQLiteHelper.class) {
                if (_instance == null) {
                    _instance = new SQLiteHelper(context);
                }
            }
        }
        return _instance;
    }

    /**
     * Count total no. instance of Readable DataBase created
     */
    public synchronized SQLiteDatabase getReadableDatabase() {
        openedConnections++;
        return getReadableDatabase();
    }

    public synchronized void close() {
        openedConnections--;
        if (openedConnections == 0) {
            close();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(Tables.BookMarkTable.createTableQuery());
            Log.i(TAG, " table created : ");
        } catch (SQLException e) {
            Log.e(TAG, " Unable to create table", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Tables.BOOKMARKDATA.getLabel());
        onCreate(db);
    }

    public static void close(Cursor cursor) {
        try {
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(TAG, " Unable to close cursur", e);
        }
    }

}
