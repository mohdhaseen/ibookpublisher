package com.artifex.mupdfdemo;

interface TextProcessor {
    void onStartLine();

    void onWord(TextWord word);

    void onEndLine();
}

