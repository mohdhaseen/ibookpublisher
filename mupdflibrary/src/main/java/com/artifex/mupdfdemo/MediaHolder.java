/**
 * 
 */
package com.artifex.mupdfdemo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;

import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;


import com.librelio.activity.SlideShowActivity;
import com.librelio.view.ImageLayout;


import java.io.File;


/**
 * The class for display all pdf's media-resources
 * 
 * @author Dmitry Valetin
 * @author Nikolay Moskvin <moskvin@netcook.org>
 */
public class MediaHolder extends FrameLayout{
	private static final String TAG = "MediaHolder";
	public static final String AUTO_PLAY_KEY = "auto_play_key";
	public static final String TRANSITION_KEY = "transition_key";
	public static final String BG_COLOR_KEY = "bg_color_key";
	public static final String FULL_PATH_KEY = "full_path_key";
	public static final String PLAY_DELAY_KEY = "play_delay_key";
	public static final String INITIAL_SLIDE_POSITION = "initial_slide_position";
	
	private Context context;
	private String basePath;
	private LinkInfoExternal linkInfo;
	private Handler autoPlayHandler;
	private GestureDetector gestureDetector;
	private ImageLayout imageLayout;


	private String uriString;
	private OnClickListener listener = null;

	
	private int autoPlayDelay;
	private boolean transition = true;
	private boolean autoPlay;
	private int bgColor;
	private String fullPath;
	


    public MediaHolder(Context context, LinkInfoExternal linkInfo, String basePath) throws IllegalStateException{
		super(context);
		this.basePath = basePath;
		this.context = context;
		this.linkInfo = linkInfo;
		this.uriString = linkInfo.url;
		
		gestureDetector = new GestureDetector(new GestureListener());
		
		if(uriString == null) {
			Log.w(TAG, "URI ??an not be empty! basePath = " + basePath);
			return;
		}

		boolean fullScreen = linkInfo.isFullScreen();

		if (linkInfo.isExternal()) {
			if (linkInfo.isImageFormat()) {
				autoPlay = linkInfo.isAutoPlay();
				autoPlayDelay = 2000;
				if(Uri.parse(uriString).getQueryParameter("wadelay") != null) {
					autoPlayDelay = Integer.valueOf(Uri.parse(uriString).getQueryParameter("wadelay"));
					autoPlay = true;
				}
				bgColor = Color.BLACK;
				if(Uri.parse(uriString).getQueryParameter("wabgcolor") != null) {
					bgColor = Uri.parse(uriString).getQueryParameter("wabgcolor").equals("white") ?
							Color.WHITE : Color.BLACK;
				}
				
				if(Uri.parse(uriString).getQueryParameter("watransition") != null) {
					transition = !Uri.parse(uriString).getQueryParameter("watransition").equals("none");
					autoPlayDelay = 1000;
					Log.d(TAG,"transition = "+transition);
				}
				//
				fullPath = basePath + Uri.parse(uriString).getPath();
				Log.d(TAG, "exist file " + fullPath + "? " + new File(fullPath).exists());
				if (fullScreen) {
					onPlaySlideOutside(basePath);
				} else {
					onPlaySlideInside(basePath);
				}
			}
		}
	}
	
	private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }


    }
	
	public void recycle() {
		Log.d(TAG,"resycle was called");
		if(autoPlayHandler!=null){
			Log.d(TAG,"removeCallbacksAndMessages");
			autoPlayHandler.removeCallbacksAndMessages(null);
		}

	}

	public LinkInfoExternal getLinkInfo() {
		return linkInfo;
	}

	public void setOnClickListener(OnClickListener l) {
		listener = l;
	}


	
	protected void onPlaySlideOutside(String basePath) {
		onPlaySlideOutside(basePath, 0);
	}
	
	protected void onPlaySlideOutside(String basePath, int position) {
		Log.d(TAG, "onPlaySlideOutside " + basePath + ", linkInfo = " + linkInfo);
		Intent intent = new Intent(getContext(), SlideShowActivity.class);
		intent.putExtra(AUTO_PLAY_KEY, autoPlay);
		intent.putExtra(TRANSITION_KEY, transition);
		intent.putExtra(BG_COLOR_KEY,bgColor);
		intent.putExtra(PLAY_DELAY_KEY,autoPlayDelay);
		intent.putExtra(FULL_PATH_KEY,fullPath);
		intent.putExtra(INITIAL_SLIDE_POSITION, position);
		getContext().startActivity(intent);
	}

	protected void onPlaySlideInside(String basePath) {
		Log.d(TAG, "onPlaySlideInside " + basePath + ", linkInfo = " + linkInfo);
		
		imageLayout = new ImageLayout(getContext(), fullPath, transition);
		post(new Runnable() {
			@Override
			public void run() {
				imageLayout.setCurrentPosition(0, false);
			}
		});
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		lp.gravity = Gravity.CENTER;
		imageLayout.setLayoutParams(lp);
		
		imageLayout.setGestureDetector(gestureDetector);
		
		if(autoPlay) {
			autoPlayHandler = new Handler();
			autoPlayHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					Log.d(TAG, "autoPlayHandler start");
					imageLayout.setCurrentPosition(imageLayout.getCurrentPosition() + 1, transition);
					autoPlayHandler.postDelayed(this, autoPlayDelay);
				}}, autoPlayDelay);
		} else {
			setVisibility(View.GONE);
		}
		
		imageLayout.setBackgroundColor(bgColor);
		
		addView(imageLayout);
		requestLayout();
	}
}

