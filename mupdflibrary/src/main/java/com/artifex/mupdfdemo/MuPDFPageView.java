package com.artifex.mupdfdemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

abstract class PassClickResultVisitor {
    public abstract void visitText(PassClickResultText result);

    public abstract void visitChoice(PassClickResultChoice result);
}

class PassClickResult {
    public final boolean changed;

    public PassClickResult(boolean _changed) {
        changed = _changed;
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
    }
}

class PassClickResultText extends PassClickResult {
    public final String text;

    public PassClickResultText(boolean _changed, String _text) {
        super(_changed);
        text = _text;
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
        visitor.visitText(this);
    }
}

class PassClickResultChoice extends PassClickResult {
    public final String[] options;
    public final String[] selected;

    public PassClickResultChoice(boolean _changed, String[] _options, String[] _selected) {
        super(_changed);
        options = _options;
        selected = _selected;
    }

    public void acceptVisitor(PassClickResultVisitor visitor) {
        visitor.visitChoice(this);
    }
}

public class MuPDFPageView extends MuPDFPageView1 {
    private static final String TAG = "MuPDFPageView";

    private final MuPDFCore mCore;


    private HashMap<String, FrameLayout> mediaHolders = new HashMap<String, FrameLayout>();
    private ArrayList<String> runningLinks;

    public MuPDFPageView(Context c, MuPDFCore muPdfCore, Point parentSize) {
        super(c, muPdfCore, parentSize);
        this.mCore = muPdfCore;
        runningLinks = new ArrayList<String>();
    }

    public int hitLinkPage(float x, float y) {
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;
        float docRelX = (x - getLeft()) / scale;
        float docRelY = (y - getTop()) / scale;
        LinkInfo[] pageLinks = mCore.getPageLinks(mPageNumber);
        for (LinkInfo pageLink : pageLinks) {
            if (pageLink instanceof LinkInfoInternal) {
                LinkInfoInternal internalLink = (LinkInfoInternal) pageLink;
                if (internalLink.rect.contains(docRelX, docRelY)) {
                    /*
                     * Here we should check the screen number against the
					 * dowble-page view and correctly recalculate it from the
					 * page number
					 */
                    int pageNumber = internalLink.pageNumber;
                    Log.d(TAG, "hitLinkPage with page = " + internalLink.pageNumber);
                    if (mCore.getDisplayPages() != 1)
                        if (pageNumber > 0)
                            return (pageNumber + 1) / 2;
                        else
                            return 0;
                    return pageNumber;
                }
            }
        }
        return -1;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        for (Map.Entry<String, FrameLayout> entry : mediaHolders.entrySet()) {
            MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
            LinkInfoExternal currentLink = mLinkHolder.getLinkInfo();
            float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

            int width = (int) ((currentLink.rect.right - currentLink.rect.left) * scale);
            int height = (int) ((currentLink.rect.bottom - currentLink.rect.top) * scale);
            // mLinkHolder.measure(widthMeasureSpec, heightMeasureSpec);
            mLinkHolder.measure(View.MeasureSpec.EXACTLY | width,
                    View.MeasureSpec.EXACTLY | height);

        }

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
                            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        float scale = mSourceScale * (float) getWidth() / (float) mSize.x;

        for (Map.Entry<String, FrameLayout> entry : mediaHolders.entrySet()) {
            MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
            LinkInfo currentLink = mLinkHolder.getLinkInfo();
            mLinkHolder.layout((int) (currentLink.rect.left * scale),
                    (int) (currentLink.rect.top * scale),
                    (int) (currentLink.rect.right * scale),
                    (int) (currentLink.rect.bottom * scale));
        }
    }

    @Override
    public void blank(int page) {
        super.blank(page);
        Iterator<Entry<String, FrameLayout>> i = mediaHolders.entrySet()
                .iterator();
        while (i.hasNext()) {
            Entry<String, FrameLayout> entry = i.next();
            MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
            mLinkHolder.recycle();
            i.remove();
            removeView(mLinkHolder);
            mLinkHolder = null;
        }
    }


    @Override
    public void removeHq() {
        super.removeHq();
        Iterator<Entry<String, FrameLayout>> i = mediaHolders.entrySet()
                .iterator();
        while (i.hasNext()) {
            Entry<String, FrameLayout> entry = i.next();
            MediaHolder mLinkHolder = (MediaHolder) entry.getValue();
            i.remove();
            removeView(mLinkHolder);
            mLinkHolder = null;
        }
    }

    @Override
    public void addHq(boolean b) {
        super.addHq(b);
        for (Map.Entry<String, FrameLayout> entry : mediaHolders.entrySet()) {
            MediaHolder mLinkHolder = (MediaHolder) entry.getValue();

            mLinkHolder.bringToFront();
        }
    }

    public void addMediaHolder(MediaHolder h, String uriString) {
        this.mediaHolders.put(uriString, h);
    }

    public void cleanRunningLinkList() {
        runningLinks.clear();
    }

    @Override
    protected Bitmap drawPage(int sizeX, int sizeY, int patchX,
                              int patchY, int patchWidth, int patchHeight) {
        return mCore.drawPage(mPageNumber, sizeX, sizeY, patchX, patchY, patchWidth, patchHeight);
    }

    @Override
    protected Bitmap updatePage(BitmapHolder h, int sizeX, int sizeY,
                                int patchX, int patchY, int patchWidth, int patchHeight) {
        return mCore.updatePage(h, mPageNumber, sizeX, sizeY, patchX, patchY, patchWidth, patchHeight);
    }

    @Override
    protected LinkInfo[] getLinkInfo() {
        return mCore.getPageLinks(mPageNumber);
    }

}
