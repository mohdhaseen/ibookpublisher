package com.artifex.mupdfdemo;

import android.graphics.RectF;

//TODO: Added multi-thread safe for singleton property
public class SearchTaskResult {
    public final String txt;
    public final int pageNumber;
    public final RectF searchBoxes[];

    private static SearchTaskResult singleton;

    static public SearchTaskResult get() {
        return singleton;
    }

    public SearchTaskResult(String txt, int pageNumber, RectF searchBoxes[]) {
        this.txt = txt;
        this.pageNumber = pageNumber;
        this.searchBoxes = searchBoxes;
    }

    static public void set(SearchTaskResult r) {
        singleton = r;
    }

    public static void recycle() {
        singleton = null;
    }
}
