package com.librelio.singletons;

import android.graphics.Bitmap;
import android.util.SparseArray;

/**
 * Created by atiqulalam on 27/10/15.
 */
public class ThumbnailSparseArray {
    //private static ThumbnailSparseArray instance;

    private static SparseArray<Bitmap> _data;

    public static SparseArray<Bitmap> getInstance() {
        if (_data == null) {
            _data = new SparseArray<Bitmap>();
        }

        return _data;
    }

    private ThumbnailSparseArray() {
        _data = new SparseArray<Bitmap>();
    }

}
