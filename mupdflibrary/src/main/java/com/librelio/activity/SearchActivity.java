package com.librelio.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.librelio.Listener.SearchPagesListener;
import com.librelio.adapter.PageNumberAdapter;
import com.librelio.task.SearchRunnable;
import com.librelio.utils.BooksSharedPreference;
import com.librelio.utils.ThemeUtils;
import com.librelio.utils.UtillArabic;
import com.niveales.wind.R;

import java.util.ArrayList;

public class SearchActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    private EditText mEditText_search;
    private ImageView mImageCross;
    private ListView mListView;
    private PageNumberAdapter mAdapter;
    private ProgressDialog mProgressDialog;
    private ArrayList<String> pageNumbers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtils.onActivityCreateSetTheme(BooksSharedPreference.getInstance(this).getTheme("GREEN"), this);
        UtillArabic.setLocale(this);
        setContentView(R.layout.activity_search);
        TextView mTextView = (TextView) findViewById(R.id.cancel_txt_id);
        mEditText_search = (EditText) findViewById(R.id.search_etext_id);
        mListView = (ListView) findViewById(R.id.search_list);
        mImageCross = (ImageView) findViewById(R.id.img_cross);
        if (getIntent().getStringExtra("search_text") != null) {
            mEditText_search.setText(getIntent().getStringExtra("search_text"));
            mAdapter = new PageNumberAdapter(SearchActivity.this, getIntent().getStringArrayListExtra("search_list"));
            mListView.setAdapter(mAdapter);
        }
        mTextView.setOnClickListener(this);
        mImageCross.setOnClickListener(this);
        mListView.setOnItemClickListener(this);
        mEditText_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 0) {
                    mImageCross.setVisibility(View.VISIBLE);
                } else {

                    mImageCross.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEditText_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                                       @Override
                                                       public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                                                           if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                                                               Log.d("Srr", v.getText().toString());
                                                               if (v.getText().length() < 1) {
                                                                   Toast.makeText(SearchActivity.this, "Please type some text to search", Toast.LENGTH_SHORT).show();
                                                                   return true;
                                                               }
                                                               mProgressDialog = ProgressDialog.show(SearchActivity.this, getResources().getString(R.string.search), getResources().getString(R.string.search));
                                                               SearchRunnable runnable = new SearchRunnable(v.getText().toString(), SearchActivity.this);
                                                               runnable.setSeeachListener(new SearchPagesListener() {
                                                                   @Override
                                                                   public void onSearchResult(final ArrayList<String> pages) {
                                                                       pageNumbers = pages;
                                                                       runOnUiThread(new Runnable() {
                                                                           @Override
                                                                           public void run() {
                                                                               mProgressDialog.dismiss();
                                                                               if (mAdapter == null) {
                                                                                   mAdapter = new PageNumberAdapter(SearchActivity.this, pages);
                                                                                   mListView.setAdapter(mAdapter);
                                                                               } else {
                                                                                   mAdapter.clear();
                                                                                   mAdapter.addAll(pages);
                                                                                   mAdapter.notifyDataSetChanged();
                                                                               }
                                                                           }
                                                                       });
                                                                   }

                                                                   @Override
                                                                   public void onSearchResultEmpty() {
                                                                       runOnUiThread(new Runnable() {
                                                                           @Override
                                                                           public void run() {
                                                                               mProgressDialog.dismiss();
                                                                               Toast.makeText(SearchActivity.this, "Not found", Toast.LENGTH_SHORT).show();
                                                                               if (mAdapter != null) {
                                                                                   mAdapter.clear();
                                                                                   mAdapter.notifyDataSetChanged();
                                                                               }
                                                                           }
                                                                       });
                                                                   }
                                                               });
                                                               Thread t = new Thread(runnable);
                                                               t.start();

                                                               return true;
                                                           }

                                                           return false;
                                                       }
                                                   }

        );
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.img_cross) {
            mEditText_search.setText("");
            if (mAdapter != null) {
                mAdapter.clear();
                mAdapter.notifyDataSetChanged();
            }
        } else if (view.getId() == R.id.cancel_txt_id) {
            finish();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent returnIntent = new Intent();
        String p = adapterView.getItemAtPosition(i).toString();
        /*if (BooksSharedPreference.getInstance(SearchActivity.this).getLanguage("ENGLISH").equals("ARABIC")) {
            p = UtillArabic.toNum(adapterView.getItemAtPosition(i).toString());
        }*/
        int pagenumber = Integer.parseInt(p);
        returnIntent.putExtra("pagenumber", pagenumber - 1);
        returnIntent.putExtra("text_to_search", mEditText_search.getText().toString());
        returnIntent.putExtra("search_found_pages", pageNumbers);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
