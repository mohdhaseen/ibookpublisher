package com.librelio.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.artifex.mupdfdemo.LinkInfoExternal;
import com.artifex.mupdfdemo.MuPDFCore;
import com.artifex.mupdfdemo.MuPDFPageAdapter;
import com.artifex.mupdfdemo.MuPDFPageView;
import com.artifex.mupdfdemo.MuPDFReaderView;
import com.artifex.mupdfdemo.SearchTaskResult;
import com.artifex.mupdfdemo.ReaderView;
import com.database.Dao;
import com.librelio.Listener.OnPageChangeListener;
import com.librelio.singletons.ThumbnailSparseArray;
import com.librelio.adapter.PDFPreviewAdapter;
import com.librelio.lib.utils.PDFParser;
import com.artifex.mupdfdemo.SearchTask;
import com.librelio.task.CreateImageRunnable;
import com.librelio.utils.BooksSharedPreference;
import com.librelio.utils.ThemeUtils;
import com.librelio.utils.UtillArabic;
import com.librelio.view.RecyclerItemClickListener;
import com.model.BookMarkModel;
import com.niveales.wind.R;

import java.util.ArrayList;

//Created by Mohd Haseen

public class MuPDFActivity extends FragmentActivity implements OnPageChangeListener, View.OnClickListener {
    private static final String TAG = "MuPDFActivity";

    private static final String FILE_NAME = "FileName";

    private static final int START_BILLING_ACTIVITY = 100;

    public static final String PREF_BUTTONS_HIDDEN = "ButtonsHidden";
    private static final int REQ_SEARCH = 1;
    private static final int REQ_BOOKMARK = 2;

    private MuPDFCore core;
    private String fileName;
    private SearchTask mSearchTask;
    private boolean buttonsVisible;
    private boolean mTopBarIsSearch;
    private ArrayList<String> pageNumbers;
    private String textToSearch;
    private ReaderView docView;
    private View buttonsView;
    private ImageView mImageAddBookMark;
    private TextView mPageNumber;
    private ViewSwitcher mTopBarSwitcher = null;
    private RelativeLayout mTopLayout;
    private FrameLayout mPreviewBarHolder;
    private RecyclerView mPreview;
    private PDFPreviewAdapter pdfPreviewPagerAdapter;
    private SparseArray<LinkInfoExternal[]> linkOfDocument;
    private LinearLayoutManager listLayoutManager;
   // private int currentlyViewing;
    private int previousPageNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ThemeUtils.onActivityCreateSetTheme(BooksSharedPreference.getInstance(this).getTheme("GREEN"), this);
        UtillArabic.setLocale(this);
        core = getMuPdfCore(savedInstanceState);

        if (core == null) {
            return;
        }


        createUI(savedInstanceState);
    }


    private MuPDFCore getMuPdfCore(Bundle savedInstanceState) {
        MuPDFCore core = null;
        if (core == null) {
            core = (MuPDFCore) getLastCustomNonConfigurationInstance();

            if (savedInstanceState != null && savedInstanceState.containsKey(FILE_NAME)) {
                fileName = savedInstanceState.getString(FILE_NAME);

            }
        }
        if (core == null) {
            Intent intent = getIntent();
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                core = openFile(Uri.decode(uri.getEncodedPath()));
                SearchTaskResult.recycle();
            }
        }

        return core;
    }

    private void createUI(Bundle savedInstanceState) {
        if (core == null)
            return;
        // Now create the UI.
        // First create the document view making use of the ReaderView's internal
        // gesture recognition
        docView = new MuPDFReaderView(this, linkOfDocument) {
            @Override
            protected void onMoveToChild(View view, int i) {
                Log.d(TAG, "onMoveToChild id = " + i);


                if (core == null) {
                    return;
                }
                MuPDFPageView pageView = (MuPDFPageView) docView.getDisplayedView();
                if (pageView != null) {
                    pageView.cleanRunningLinkList();
                }
                super.onMoveToChild(view, i);
                setCurrentlyViewedPreview();

            }


            @Override
            protected void onContextMenuClick() {
                if (!buttonsVisible) {
                    showButtons();
                } else {
                    hideButtons();

                }
            }

        };
        MuPDFPageAdapter mDocViewAdapter = new MuPDFPageAdapter(this, core);
        docView.setAdapter(mDocViewAdapter);
        docView.setOnPageChangeListener(this);

        // Make the buttons overlay, and store all its
        // controls in variables
        makeButtonsView();


        // Reinstate last state if it was recorded

        int pageNum = 0;
        if (getIntent() != null && getIntent().getExtras() != null)
            pageNum = getIntent().getIntExtra("page_number", 1) - 1;

        if (savedInstanceState != null && savedInstanceState.containsKey(FILE_NAME)) {
            pageNum = savedInstanceState.getInt("page_number");
        }
        docView.setDisplayedViewIndex(pageNum);

        // Give preview thumbnails time to appear before showing bottom bar
        if (savedInstanceState == null
                || !savedInstanceState.getBoolean(PREF_BUTTONS_HIDDEN, false)) {
            mPreview.postDelayed(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showButtons();
                        }
                    });
                }
            }, 0);
        }

        // Stick the document view and the buttons overlay into a parent view
        RelativeLayout layout = new RelativeLayout(this);
        layout.addView(docView);
        layout.addView(buttonsView);

        setContentView(layout);

        mSearchTask = new SearchTask(this, core) {
            @Override
            protected void onTextFound(SearchTaskResult result) {
                SearchTaskResult.set(result);
                // Ask the ReaderView to move to the resulting page
                docView.setDisplayedViewIndex(result.pageNumber);
                // Make the ReaderView act on the change to SearchTaskResult
                // via overridden onChildSetup method.
                Log.d("PageNumber", "" + result.pageNumber);
                docView.resetupChildren();
            }
        };
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == START_BILLING_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }

        if (requestCode == REQ_BOOKMARK) {
            if (resultCode == RESULT_OK) {
                docView.setDisplayedViewIndex(data.getIntExtra("pagenumber", 0));
                if (isBookMarked(docView.getCurrentPage() + 1)) {
                    mImageAddBookMark.setImageResource(R.mipmap.red_bookmark);
                    mImageAddBookMark.setSelected(true);
                } else {
                    mImageAddBookMark.setImageResource(R.mipmap.gray_bookmark);
                    mImageAddBookMark.setSelected(false);
                }
            }
        }
        if (requestCode == REQ_SEARCH) {
            if (resultCode == RESULT_OK) {
                docView.setDisplayedViewIndex(data.getIntExtra("pagenumber", 0));
                search(data.getStringExtra("text_to_search"));
                textToSearch = data.getStringExtra("text_to_search");
                pageNumbers = data.getStringArrayListExtra("search_found_pages");
            } else {
                SearchTaskResult.recycle();
                docView.setDisplayedViewIndex(docView.getCurrentPage());
                textToSearch = null;
                pageNumbers = null;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        MuPDFCore mycore = core;
        core = null;
        return mycore;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (fileName != null && docView != null) {
            outState.putString("FileName", fileName);
            outState.putInt("page_number", docView.getDisplayedViewIndex());
            // Store current page in the prefs against the file name,
            // so that we can pick it up each time the file is loaded
            // Other info is needed only for screen-orientation change,
            // so it can go in the bundle
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("page" + fileName, docView.getDisplayedViewIndex());
            // edit.putInt(PREF_SAVED_ORIENTATION, mOrientation);
            edit.apply();
        }

        if (!buttonsVisible)
            outState.putBoolean(PREF_BUTTONS_HIDDEN, true);


    }

    @Override
    protected void onPause() {
        super.onPause();


        if (fileName != null && docView != null) {
            SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putInt("page" + fileName, docView.getDisplayedViewIndex());

            edit.apply();
        }
    }

    @Override
    public void onDestroy() {
        if (core != null) {
            core.onDestroy();
        }
        core = null;

        super.onDestroy();
    }

    void showButtons() {

        if (core == null) {
            return;
        }

        if (!buttonsVisible) {
            mImageAddBookMark.setVisibility(View.VISIBLE);
            if (isBookMarked(docView.getCurrentPage() + 1)) {
                mImageAddBookMark.setImageResource(R.mipmap.red_bookmark);
                mImageAddBookMark.setSelected(true);
            } else {
                mImageAddBookMark.setImageResource(R.mipmap.gray_bookmark);
                mImageAddBookMark.setSelected(false);
            }
            mPageNumber.setVisibility(View.VISIBLE);
            mPageNumber.setText(docView.getCurrentPage() + 1 + " of " + core.countPages());
            buttonsView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            buttonsVisible = true;
            Animation anim = new TranslateAnimation(0, 0, -mTopLayout.getHeight(), 0);
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {

                    mTopLayout.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });

            mTopLayout.startAnimation(anim);
            // Don't show thumbnail if not requested

            if (!BooksSharedPreference.getInstance(MuPDFActivity.this).getThumbnailFeature("NO").equals("YES")) {
                return;
            }
            // Update listView position
           // centerPreviewAtPosition(currentlyViewing);
            anim = new TranslateAnimation(0, 0, mPreviewBarHolder.getHeight(), 0);
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mPreviewBarHolder.setVisibility(View.VISIBLE);

                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {

                }
            });
            mPreviewBarHolder.startAnimation(anim);
        }
    }

    void hideButtons() {
        if (buttonsVisible) {
            mImageAddBookMark.setVisibility(View.INVISIBLE);
            mPageNumber.setVisibility(View.GONE);
            buttonsView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            buttonsVisible = false;


            //Animation anim = new TranslateAnimation(0, 0, 0, -mTopBarSwitcher.getHeight());
            Animation anim = new TranslateAnimation(0, 0, 0, -mTopLayout.getHeight());
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {

                    mTopLayout.setVisibility(View.INVISIBLE);
                }
            });

            mTopLayout.startAnimation(anim);
            // Don't show thumbnail if not requested

            if (!BooksSharedPreference.getInstance(MuPDFActivity.this).getThumbnailFeature("NO").equals("YES")) {
                return;
            }
            anim = new TranslateAnimation(0, 0, 0, this.mPreviewBarHolder.getHeight());
            anim.setDuration(200);
            anim.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    mPreviewBarHolder.setVisibility(View.INVISIBLE);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });
            mPreviewBarHolder.startAnimation(anim);
        }
    }

    void searchModeOff() {
        if (mTopBarIsSearch) {
            mTopBarIsSearch = false;
            mTopBarSwitcher.showPrevious();
            SearchTaskResult.recycle();
            // Make the ReaderView act on the change to mSearchTaskResult
            // via overridden onChildSetup method.
            docView.resetupChildren();
        }
    }

    @SuppressLint("InflateParams")
    void makeButtonsView() {
        buttonsView = getLayoutInflater().inflate(R.layout.buttons, null);
        mPreviewBarHolder = (FrameLayout) buttonsView.findViewById(R.id.PreviewBarHolder);
        mPreview = new RecyclerView(this);
        listLayoutManager = new LinearLayoutManager(this);
        listLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPreview.setLayoutManager(listLayoutManager);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams
                .WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mPreview.setLayoutParams(lp);
        pdfPreviewPagerAdapter = new PDFPreviewAdapter(this, core);
        mPreview.setAdapter(pdfPreviewPagerAdapter);
        if (ThumbnailSparseArray.getInstance().size() < core.countPages()) {
            Thread thread = new Thread(new CreateImageRunnable(pdfPreviewPagerAdapter, core.countPages()));
            thread.start();
        }
        mPreview.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        hideButtons();
                        docView.setDisplayedViewIndex(position);
                        mPageNumber.setVisibility(View.VISIBLE);
                        mPageNumber.setText(position + 1 + " of " + core.countPages());

                    }
                })
        );


        mPreviewBarHolder.addView(mPreview);
        ImageView mImageSearch = (ImageView) buttonsView.findViewById(R.id.img_search);
        if (!BooksSharedPreference.getInstance(this).getSearchCapability("NO").equals("YES")) {
            mImageSearch.setVisibility(View.INVISIBLE);
        }
        ImageView mImageBack = (ImageView) buttonsView.findViewById(R.id.img_back);
        ImageView mImageBookMark = (ImageView) buttonsView.findViewById(R.id.img_book_mark);
        if (!BooksSharedPreference.getInstance(this).getBookMarkFeature("NO").equals("YES")) {
            mImageBookMark.setVisibility(View.INVISIBLE);
        }
        mImageAddBookMark = (ImageView) buttonsView.findViewById(R.id.add_bookmark);
        mImageAddBookMark.setOnClickListener(this);
        mImageSearch.setOnClickListener(this);
        mImageBookMark.setOnClickListener(this);
        mImageBack.setOnClickListener(this);
        mPageNumber = (TextView) buttonsView.findViewById(R.id.page_no);
        mTopLayout = (RelativeLayout) buttonsView.findViewById(R.id.top_layout);

        mTopLayout.setVisibility(View.VISIBLE);
        if (BooksSharedPreference.getInstance(MuPDFActivity.this).getThumbnailFeature("NO").equals("YES")) {
            mPreviewBarHolder.setVisibility(View.VISIBLE);
        }
    }


    private void search(String text) {
        int displayPage = docView.getDisplayedViewIndex();
        SearchTaskResult r = SearchTaskResult.get();
        int searchPage = r != null ? r.pageNumber : -1;
        mSearchTask.go(text, getDirection(), displayPage, searchPage);
    }

    private int getDirection() {
        if (previousPageNumber < docView.getCurrentPage()) {
            previousPageNumber = docView.getCurrentPage();
            return 1;
        }
        previousPageNumber = docView.getCurrentPage();
        return -1;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (buttonsVisible && !mTopBarIsSearch) {
            hideButtons();
        } else {
            showButtons();
            searchModeOff();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private MuPDFCore openFile(String path) {
        int lastSlashPos = path.lastIndexOf('/');
        fileName = lastSlashPos == -1
                ? path
                : path.substring(lastSlashPos + 1);
        Log.d(TAG, "Trying to open " + path);
        PDFParser linkGetter = new PDFParser(this, path);
        linkOfDocument = linkGetter.getLinkInfo();

        try {
            core = new MuPDFCore(path);
            // New file: drop the old outline data
            //set Page number

        } catch (Exception e) {
            Log.e(TAG, "get core failed", e);
            return null;
        }
        return core;
    }

    private void setCurrentlyViewedPreview() {
        int i = docView.getDisplayedViewIndex();
        if (core.getDisplayPages() == 2) {
            i = (i * 2) - 1;
        }
      //  pdfPreviewPagerAdapter.setCurrentlyViewing(i);
       // currentlyViewing = i;
    }

    public void centerPreviewAtPosition(int position) {
        int offset = (mPreview.getWidth() / 2) -
                (getResources().getDimensionPixelSize(R.dimen.page_preview_size_width) / 2);
      //  listLayoutManager.scrollToPositionWithOffset(position, offset);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pdf_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageChange(int pageNumber) {
        mPageNumber.setVisibility(View.VISIBLE);
        mPageNumber.setText(pageNumber + " of " + core.countPages());
        if (isBookMarked(pageNumber)) {
            mImageAddBookMark.setImageResource(R.mipmap.red_bookmark);
            mImageAddBookMark.setSelected(true);
        } else {
            mImageAddBookMark.setSelected(false);
            mImageAddBookMark.setImageResource(R.mipmap.gray_bookmark);
        }
        if (pageNumbers != null && pageNumbers.size() > 0 && isToSearch()) {
            search(textToSearch);
        }


    }


    private boolean isBookMarked(int pageNumber) {

        for (BookMarkModel p : Dao.getInstance(this).getBookMarks()) {
            if (p.getPageNumber() == pageNumber)
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.img_search) {
            Intent searchIntent = new Intent(this, SearchActivity.class);
            if (pageNumbers != null && pageNumbers.size() > 0) {
                searchIntent.putExtra("search_text", textToSearch);
                searchIntent.putExtra("search_list", pageNumbers);
            }
            mPageNumber.setVisibility(View.INVISIBLE);
            startActivityForResult(searchIntent, REQ_SEARCH);

        } else if (i == R.id.img_book_mark) {
            Intent gridIntent = new Intent(this, GridViewActivity.class);
            mPageNumber.setVisibility(View.INVISIBLE);
            startActivityForResult(gridIntent, REQ_BOOKMARK);
        } else if (i == R.id.add_bookmark) {
            if (mImageAddBookMark.isSelected()) {
                Toast.makeText(this, "Book Mark Removed ", Toast.LENGTH_SHORT).show();
                Dao.getInstance(this).deleteBookMark(docView.getCurrentPage() + 1);
                mImageAddBookMark.setImageResource(R.mipmap.gray_bookmark);
                mImageAddBookMark.setSelected(false);
            } else {
                Toast.makeText(this, "Book Mark Saved Page no. " + mPageNumber.getText(), Toast.LENGTH_SHORT).show();
                Dao.getInstance(this).insertBookMark(new BookMarkModel(docView.getCurrentPage() + 1));
                mImageAddBookMark.setImageResource(R.mipmap.red_bookmark);
                mImageAddBookMark.setSelected(true);
            }
        } else if (i == R.id.img_back) {
            finish();
        }
    }

    public boolean isToSearch() {
        String p = String.valueOf((docView.getCurrentPage() + 1));
        return pageNumbers.contains(p);

    }


}
