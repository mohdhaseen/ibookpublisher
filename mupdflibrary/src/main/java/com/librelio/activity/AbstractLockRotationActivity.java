package com.librelio.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import com.librelio.utils.SystemHelper;

public abstract class AbstractLockRotationActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//noinspection ResourceType
		setRequestedOrientation(
				SystemHelper.getScreenOrientation(this));
	}	
	
}
