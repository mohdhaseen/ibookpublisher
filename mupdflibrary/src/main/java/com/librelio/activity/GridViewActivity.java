package com.librelio.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.database.Dao;
import com.librelio.singletons.ThumbnailSparseArray;
import com.librelio.adapter.BookMarkedPagesAdapter;
import com.librelio.adapter.ThumbnailGridAdapter;
import com.librelio.utils.BooksSharedPreference;
import com.librelio.utils.ThemeUtils;
import com.librelio.utils.UtillArabic;
import com.model.BookMarkModel;
import com.niveales.wind.R;

/**
 * Created by Mohd Haseen on 27/10/15.
 */
public class GridViewActivity extends FragmentActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private GridView gridView;
    private ThumbnailGridAdapter gridAdapter;
    private ImageView img_left, img_right;
    private TextView txtDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeUtils.onActivityCreateSetTheme(BooksSharedPreference.getInstance(this).getTheme("GREEN"), this);
        UtillArabic.setLocale(this);
        setContentView(R.layout.gridview_layout);
        gridView = (GridView) findViewById(R.id.gridView);
        txtDone = (TextView) findViewById(R.id.txt_done);
        gridAdapter = new ThumbnailGridAdapter(ThumbnailSparseArray.getInstance(), Dao.getInstance(this).getBookMarks(), R.layout.grid_item, this);
        gridView.setAdapter(gridAdapter);

        initViews();
        img_left.setSelected(true);
        img_right.setSelected(false);
        addUIListener();
    }

    private void addUIListener() {
        img_right.setOnClickListener(this);
        img_left.setOnClickListener(this);
        gridView.setOnItemClickListener(this);
        txtDone.setOnClickListener(this);
    }

    private void initViews() {
        img_left = (ImageView) findViewById(R.id.grid_imageview_left);
        img_right = (ImageView) findViewById(R.id.grid_imageview_right);

    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.grid_imageview_left) {
            if (!img_left.isSelected()) {
                img_left.setSelected(true);
                img_left.setImageResource(R.mipmap.selected_grid);
                img_right.setSelected(false);
                img_right.setImageResource(R.mipmap.unselected_bookmark);
                gridAdapter = new ThumbnailGridAdapter(ThumbnailSparseArray.getInstance(), Dao.getInstance(this).getBookMarks(), R.layout.grid_item, this);
                gridView.setAdapter(gridAdapter);
            }

        } else if (i == R.id.grid_imageview_right) {
            if (!img_right.isSelected()) {
                img_right.setSelected(true);
                img_right.setImageResource(R.mipmap.selected_bookmark);
                img_left.setSelected(false);
                img_left.setImageResource(R.mipmap.unselected_grid);
                BookMarkedPagesAdapter bookMarkedPagesAdapter = new BookMarkedPagesAdapter(getBookMarkedPages(ThumbnailSparseArray.getInstance()), R.layout.grid_item, this);
                gridView.setAdapter(bookMarkedPagesAdapter);

            }

        } else if (i == R.id.txt_done) {
            finish();
        }
    }

    private SparseArray<Bitmap> getBookMarkedPages(SparseArray<Bitmap> allPages) {
        SparseArray<Bitmap> pages = new SparseArray<>();
        for (int i = 0; i < allPages.size(); i++) {
            if (isBookMarked(i + 1)) {
                pages.put(i + 1, allPages.get(i));

            }
        }
        return pages;
    }

    private boolean isBookMarked(int pagenumber) {
        for (BookMarkModel p : Dao.getInstance(this).getBookMarks()) {
            if (p.getPageNumber() == pagenumber)
                return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ListAdapter adapter = gridView.getAdapter();
        Intent returnIntent = new Intent();

        if (adapter instanceof ThumbnailGridAdapter) {
            Toast.makeText(this, "" + (l + 1), Toast.LENGTH_SHORT).show();
            returnIntent.putExtra("pagenumber", (int) l);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } else if (adapter instanceof BookMarkedPagesAdapter) {
            Toast.makeText(this, "" + l, Toast.LENGTH_SHORT).show();
            returnIntent.putExtra("pagenumber", (int) l - 1);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }
}
