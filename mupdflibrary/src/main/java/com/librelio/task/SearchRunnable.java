package com.librelio.task;

import android.content.Context;
import android.graphics.RectF;
import android.util.Log;

import com.artifex.mupdfdemo.MuPDFCore;
import com.librelio.Listener.SearchPagesListener;
import com.librelio.utils.BooksSharedPreference;
import com.librelio.utils.UtillArabic;

import java.util.ArrayList;

/**
 * Created by craterzone on 9/11/15.
 */
public class SearchRunnable implements Runnable {


    private final Context mContext;
    private String text;
    private SearchPagesListener mliSearchPagesListener;

    public SearchRunnable(String text, Context context) {
        mContext = context;
        this.text = text;
    }

    @Override
    public void run() {

        searchAll(text);
    }

    public void searchAll(String text) {

        MuPDFCore core = null;
        try {
            core = new MuPDFCore("/data/data/com.books/files/pdf.pdf");
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < core.countPages(); i++) {
            RectF searchHits[] = core.searchPage(i, text);
            if (searchHits.length > 0)
                list.add((i + 1) + "");
        }
        Log.d("Search result", "" + list);
        if (list.size() > 0) {
           /* if (BooksSharedPreference.getInstance(mContext).getLanguage("ENGLISH").equals("ARABIC")) {
                ArrayList<String> list1 = new ArrayList<String>();
                for (int i = 0; i < list.size(); i++) {
                    list1.add(i, UtillArabic.toArabic(list.get(i)));
                }
                list.clear();
                list.addAll(list1);
            }*/
            mliSearchPagesListener.onSearchResult(list);
        } else {
            mliSearchPagesListener.onSearchResultEmpty();
        }
    }

    public void setSeeachListener(SearchPagesListener listener) {
        this.mliSearchPagesListener = listener;

    }
}
