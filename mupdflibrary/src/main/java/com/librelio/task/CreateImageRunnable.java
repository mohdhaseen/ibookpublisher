package com.librelio.task;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseArray;

import com.librelio.singletons.ThumbnailSparseArray;
import com.librelio.adapter.PDFPreviewAdapter;

/**
 * Created by craterzone on 27/11/15.
 */
public class CreateImageRunnable implements Runnable {
    private PDFPreviewAdapter mPdfPreviewAdapter;
    private final SparseArray<Bitmap> mBitmapCache = ThumbnailSparseArray.getInstance();
    private int size;

    public CreateImageRunnable(PDFPreviewAdapter pdfPreviewPagerAdapter,int size) {
        this.mPdfPreviewAdapter = pdfPreviewPagerAdapter;
        this.size=size;
    }

    @Override
    public void run() {
        Log.i("start time", System.currentTimeMillis() + "");
        for (int i = 0; i <size; i++) {
            Bitmap lq = mPdfPreviewAdapter.getCachedBitmap(i);
            mBitmapCache.put(i, lq);
            Log.i("start time", System.currentTimeMillis() + "");
            Log.i("size=", mBitmapCache.size() + "");
        }

    }
}
