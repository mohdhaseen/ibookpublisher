package com.librelio.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.model.BookMarkModel;
import com.niveales.wind.R;

import java.util.ArrayList;


/**
 * Created by atiqulalam on 27/10/15.
 */
public class BookMarkedPagesAdapter extends ArrayAdapter {
    private SparseArray<Bitmap> mData;
    private Context mContext;
    private int layoutResourceId;

    public BookMarkedPagesAdapter(SparseArray<Bitmap> data, int layoutResourceId, Context context) {
        super(context, 0);
        this.mData = data;
        this.mContext = context;
        this.layoutResourceId = layoutResourceId;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Bitmap getItem(int position) {
        return mData.valueAt(position);
    }

    @Override
    public long getItemId(int position) {
        return mData.keyAt(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        Bitmap bm = getItem(position);
        holder.imageView.setImageBitmap(bm);

        return row;
    }


    static class ViewHolder {
        ImageView imageView;
    }
}