package com.librelio.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.model.BookMarkModel;
import com.niveales.wind.R;

import java.util.ArrayList;


/**
 * Created by atiqulalam on 27/10/15.
 */
public class ThumbnailGridAdapter extends ArrayAdapter {
    private SparseArray<Bitmap> mData;
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<BookMarkModel> markedPages;

    public ThumbnailGridAdapter(SparseArray<Bitmap> data, ArrayList<BookMarkModel> pages, int layoutResourceId, Context context) {
        super(context, 0);
        this.mData = data;
        this.mContext = context;
        this.layoutResourceId = layoutResourceId;
        this.markedPages = pages;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Bitmap getItem(int position) {
        return mData.valueAt(position);
    }

    @Override
    public long getItemId(int position) {
        return mData.keyAt(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (isBookMarked(position + 1)) {
            row.findViewById(R.id.icon_book_mark).setVisibility(View.VISIBLE);
        } else {
            row.findViewById(R.id.icon_book_mark).setVisibility(View.GONE);
        }
        Bitmap bm = getItem(position);
        holder.imageView.setImageBitmap(bm);

        return row;
    }

    private boolean isBookMarked(int pagenumber) {
        for (BookMarkModel p : markedPages) {
            if (p.getPageNumber() == pagenumber)
                return true;
        }
        return false;
    }

    static class ViewHolder {
        ImageView imageView;
    }
}