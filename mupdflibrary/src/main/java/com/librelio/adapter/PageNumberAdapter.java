package com.librelio.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.niveales.wind.R;

import java.util.List;


/**
 * Created by craterzone on 15/10/15.
 */
public class PageNumberAdapter extends ArrayAdapter<String> {

    public class ViewHolder {
        TextView textViewName;

    }


    private Context mContext;

    public PageNumberAdapter(Context context, List<String> resource) {
        super(context, 0, resource);
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        String item = getItem(position);
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            rowView = inflater.inflate(R.layout.page_row, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.textViewName = (TextView) rowView.findViewById(R.id.page_no);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.textViewName.setText(item);
        return rowView;
    }


    @Override
    public String getItem(int position) {
        return super.getItem(position);
    }


}
