package com.librelio.utils;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by craterzone on 14/12/15.
 */
public class UtillArabic {
    public static String toArabic(String s) {
        int arabic_zero_unicode = 1632;
        String str = s;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < str.length(); ++i) {
            builder.append((char) ((int) str.charAt(i) - 48 + arabic_zero_unicode));
        }

        return builder.reverse().toString();
    }

    public static void setLocale(Context c) {
        if (BooksSharedPreference.getInstance(c).getLanguage("ENGLISH").equals("ARABIC")) {
            Locale locale = new Locale("ar");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            c.getResources().updateConfiguration(config, c.getResources().getDisplayMetrics());
        }
        if (BooksSharedPreference.getInstance(c).getLanguage("ENGLISH").equals("ENGLISH")) {
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            c.getResources().updateConfiguration(config, c.getResources().getDisplayMetrics());
        }
    }

    public static String toNum(String s) {
        int arabic_zero_unicode = 1632;
        String str = s;
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < str.length(); ++i) {
            builder.append((char) ((int) str.charAt(i) + 48 - arabic_zero_unicode));
        }

        return builder.reverse().toString();
    }

}

