package com.librelio.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class BooksSharedPreference {
    public static final String TAG = BooksSharedPreference.class.getName();
    private static SharedPreferences _pref;
    private static BooksSharedPreference _instance;
    private static final int PRIVATE_MODE = 0;
    public static final String SHARED_PREF_NAME = "comvigo";

    private BooksSharedPreference() {
    }

    private enum Keys {
        USER_ID("user_id"), USER_PASSWORD("user_password"), IS_LOGIN("isLogin"), COMPUTER_ID(
                "computer_id"), USER_ID_FIELD("user_id_field"), SETTING_ID(
                "setting_id"), SETTING_NAME("setting_name"), DEEP_HASH_CODE(
                "deep_hash_code"), FLAG_SETTING_CHANGED("flag"), TIMESTAMP("timestamp");

        private String label;

        private Keys(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public static BooksSharedPreference getInstance(Context context) {
        if (_pref == null) {
            _pref = context
                    .getSharedPreferences(SHARED_PREF_NAME, PRIVATE_MODE);
        }
        if (_instance == null) {
            _instance = new BooksSharedPreference();
        }
        return _instance;
    }

    /**
     * This Method Clear shared preference.
     */
    public void clear() {
        Editor editor = _pref.edit();
        editor.clear();

        editor.commit();
    }

    public void setTheme(String value) {
        setString(Keys.USER_ID.getLabel(), value);
    }

    public String getTheme(String defaultValue) {
        return getString(Keys.USER_ID.getLabel(), defaultValue);
    }

    public void setLanguage(String value) {
        setString(Keys.SETTING_ID.getLabel(), value);
    }

    public String getLanguage(String defaultValue) {
        return getString(Keys.SETTING_ID.getLabel(), defaultValue);
    }

    public void setSearchCapability(String value) {
        setString(Keys.SETTING_NAME.getLabel(), value);
    }

    public String getSearchCapability(String defaultValue) {
        return getString(Keys.SETTING_NAME.getLabel(), defaultValue);
    }


    public void setBookMarkFeature(String value) {
        setString(Keys.USER_ID_FIELD.getLabel(), value);
    }

    public String getBookMarkFeature(String defaultValue) {
        return getString(Keys.USER_ID_FIELD.getLabel(), defaultValue);
    }

    public void setThumbnailFeature(String value) {
        setString(Keys.COMPUTER_ID.getLabel(), value);
    }

    public String getThumbnailFeature(String defaultValue) {
        return getString(Keys.COMPUTER_ID.getLabel(), defaultValue);
    }

    public String getUserPassword(String defaultValue) {
        return getString(Keys.USER_PASSWORD.getLabel(), defaultValue);
    }

    public void setUserPassword(String value) {
        setString(Keys.USER_PASSWORD.getLabel(), value);
    }

    private void setString(String key, String value) {
        if (key != null && value != null) {
            try {
                if (_pref != null) {
                    Editor editor = _pref.edit();
                    editor.putString(key, value);
                    editor.commit();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private void setInt(String key, int value) {
        if (key != null) {
            try {
                if (_pref != null) {
                    Editor editor = _pref.edit();
                    editor.putInt(key, value);
                    editor.commit();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private String getString(String key, String defaultValue) {
        if (_pref != null && key != null && _pref.contains(key)) {
            return _pref.getString(key, defaultValue);
        }
        return defaultValue;
    }

    private int getInt(String key, int defaultValue) {
        if (_pref != null && key != null && _pref.contains(key)) {
            return _pref.getInt(key, defaultValue);
        }
        return defaultValue;
    }

    public void setLogin(boolean isLogin) {
        setBoolean(Keys.IS_LOGIN.getLabel(), isLogin);
    }

    public boolean islogin(boolean defaultValue) {
        return getBoolean(Keys.IS_LOGIN.getLabel(), defaultValue);
    }

    private void setBoolean(String key, boolean value) {
        if (key != null) {
            try {
                if (_pref != null) {
                    Editor editor = _pref.edit();
                    editor.putBoolean(key, value);
                    editor.commit();
                }
            } catch (Exception e) {
                Log.e(TAG, "Unable to set " + key + "= " + value
                        + "in shared preference", e);
            }
        }
    }

    private boolean getBoolean(String key, boolean defaultValue) {
        if (_pref != null && key != null && _pref.contains(key)) {
            return _pref.getBoolean(key, defaultValue);
        }
        return defaultValue;
    }

}
