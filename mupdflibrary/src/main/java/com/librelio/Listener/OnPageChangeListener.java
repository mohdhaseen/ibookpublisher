package com.librelio.Listener;

/**
 * Created by craterzone on 3/11/15.
 */
public interface OnPageChangeListener {
  void  onPageChange(int pageNumber);
}
