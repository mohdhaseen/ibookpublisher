package com.librelio.Listener;

import java.util.ArrayList;

/**
 * Created by craterzone on 9/11/15.
 */
public interface SearchPagesListener {
    void onSearchResult(ArrayList<String> pages);

    void onSearchResultEmpty();
}
