package com.books.models;

/**
 * Created by craterzone on 29/10/15.
 */
public class TableOfContents {

    private String pageNumber;
    private String chapterName;

    public TableOfContents(String pageNumber, String chapterName) {
        this.pageNumber = pageNumber;
        this.chapterName = chapterName;
    }

    public String getChapterName() {
        return chapterName;
    }


    public String getPageNumber() {
        return pageNumber;
    }


}
