package com.books.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.books.R;
import com.librelio.utils.BooksSharedPreference;

/**
 * Created by craterzone on 29/10/15.
 */
public class FragmentCoverPage extends Fragment implements View.OnClickListener {
    View mView;
    ImageView cPage;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_cover_page, container, false);
        cPage = (ImageView) mView.findViewById(R.id.cv_page);
        cPage.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View view) {
        mView.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_current_to_letf));
        mView.setVisibility(View.GONE);
        getFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in_right, 0, 0, R.anim.slide_out_right
        ).replace(R.id.container, new FragmentTableContent(), "TemplateFragment").addToBackStack(null).commit();
        BooksSharedPreference.getInstance(getActivity()).setLogin(true);
    }
}
