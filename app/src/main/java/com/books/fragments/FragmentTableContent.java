package com.books.fragments;

import com.books.R;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.books.BookApplication;
import com.books.adapter.TableContentsAdapter;
import com.books.models.TableOfContents;

import java.util.ArrayList;

/**
 * Created by craterzone on 29/10/15.
 */
public class FragmentTableContent extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_table_content, container, false);


        String chapters[] = getResources().getStringArray(R.array.chapter_name);
        String pagenumber[] = getResources().getStringArray(R.array.page_number);
        ArrayList<TableOfContents> list = new ArrayList<>();
        for (int i = 0; i < chapters.length; i++) {
            list.add(new TableOfContents(pagenumber[i], chapters[i]));
        }
        TableContentsAdapter adapter = new TableContentsAdapter(getActivity(), list);
        ListView listView = (ListView) mView.findViewById(R.id.listView_contents);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        mView.findViewById(R.id.img_back).setOnClickListener(this);

        return mView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                getFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        int pageNumber = Integer.parseInt(((TableOfContents) adapterView.getItemAtPosition(i)).getPageNumber());
        Uri uri = Uri.parse("file://" + getActivity().getFilesDir() + "/pdf.pdf");
        try {
            BookApplication.startPDFActivity(getActivity(), uri, true, pageNumber);
        } catch (Exception e) {
            Log.e("FragmentTableContent", "Problem with starting PDF-activity, path: " + uri.getPath(), e);
        }

    }

}
