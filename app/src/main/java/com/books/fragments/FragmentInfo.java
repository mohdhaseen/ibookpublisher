package com.books.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.books.R;

/**
 * Created by craterzone on 30/10/15.
 */
public class FragmentInfo extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        ((TextView)view.findViewById(R.id.txt1)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView)view.findViewById(R.id.txt1)).setText(Html.fromHtml(getResources().getString(R.string.company_name)));
        ((TextView)view.findViewById(R.id.info_Txt_view)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView)view.findViewById(R.id.info_Txt_view)).setText(Html.fromHtml(getResources().getString(R.string.disclaimer)));
        return view;
    }
}
