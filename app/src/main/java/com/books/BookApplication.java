package com.books;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.librelio.activity.MuPDFActivity;
import com.librelio.utils.BooksSharedPreference;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;


public class BookApplication extends Application {

    private static final String TAG = "BookApplication";
    private static final String PATH_SEPARATOR = "/";

    private static String baseUrl;


    @NonNull
    private static BookApplication instance;

    public BookApplication() {
        instance = this;
    }

    public static BookApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //   baseUrl = "http://librelio-europe.s3.amazonaws.com/" + getClientName(this) + PATH_SEPARATOR + getMagazineName(this) + PATH_SEPARATOR;
//set Features from book info
        BooksSharedPreference.getInstance(this).setTheme(getResources().getString(R.string.theme_name));
        BooksSharedPreference.getInstance(this).setLanguage(getResources().getString(R.string.selected_language));
        BooksSharedPreference.getInstance(this).setSearchCapability(getResources().getString(R.string.search_capability));
        BooksSharedPreference.getInstance(this).setBookMarkFeature(getResources().getString(R.string.bookmark_feature));
        BooksSharedPreference.getInstance(this).setThumbnailFeature(getResources().getString(R.string.thumbnail_feature));
        File file = new File("file://" + getFilesDir() + "/" + getResources().getString(R.string.content_file_name));
        if (file.length() == 0) {
            PDFFileCopyandReadAssets();
        } else {
            Log.d("file size-", file.length() + "");
        }

    }

    public static void startPDFActivity(Context context, Uri uri, boolean showThumbnails, int pageNumber) {
        try {
            Intent intent = new Intent(context, MuPDFActivity.class);
            intent.setAction(Intent.ACTION_VIEW);
            intent.setData(uri);
            intent.putExtra("page_number", pageNumber);
            context.startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "Problem with starting PDF-activity, path: " + uri.getPath(), e);
        }

    }


    private void PDFFileCopyandReadAssets() {
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        File file = new File(getFilesDir(), "pdf.pdf");
        try {
            in = assetManager.open("data/pdf.pdf");
            out = openFileOutput(file.getName(), Context.MODE_WORLD_READABLE);

            readFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }
    }

    private void readFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


}
