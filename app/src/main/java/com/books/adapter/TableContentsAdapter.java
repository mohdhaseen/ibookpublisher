package com.books.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.books.R;
import com.books.models.TableOfContents;

import java.util.List;


/**
 * Created by craterzone on 15/10/15.
 */
public class TableContentsAdapter extends ArrayAdapter<TableOfContents> {

    public class ViewHolder {
        TextView textViewChapter;
        TextView textViewPage;
    }


    private Context mContext;

    public TableContentsAdapter(Context context, List<TableOfContents> resource) {
        super(context, 0, resource);
        this.mContext = context;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        TableOfContents item = getItem(position);
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            rowView = inflater.inflate(R.layout.table_of_content_row, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.textViewChapter = (TextView) rowView.findViewById(R.id.chapter_name);
            viewHolder.textViewPage = (TextView) rowView
                    .findViewById(R.id.page_number);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.textViewChapter.setText(item.getChapterName());
        holder.textViewPage.setText(item.getPageNumber());
        return rowView;
    }


}
