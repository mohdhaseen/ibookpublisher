package com.books.avtivty;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.books.R;

/**
 * Created by craterzone on 26/11/15.
 */
public class SplashActivity extends Activity {
    private Thread mThread = new Thread() {
        @Override
        public void run() {
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            goToHome();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        mThread.start();
    }

    private void goToHome() {
        this.startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}
