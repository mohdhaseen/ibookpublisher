package com.books.avtivty;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.books.R;
import com.books.fragments.FragmentAuthor;
import com.books.fragments.FragmentBlurb;
import com.books.fragments.FragmentCoverPage;
import com.books.fragments.FragmentInfo;
import com.books.fragments.FragmentTableContent;
import com.books.utils.ThemeUtil;
import com.librelio.utils.ConstantUtill;
import com.librelio.utils.BooksSharedPreference;
import com.librelio.utils.UtillArabic;

//Please ignore comments because code is not complete
public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    TextView mteTextViewBook;
    TextView mTextAuthor;
    TextView mTextBlurb;
    TextView mTextInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ThemeUtil.onActivityCreateSetTheme(BooksSharedPreference.getInstance(this).getTheme("GREEN"), this);
        super.onCreate(savedInstanceState);
        UtillArabic.setLocale(this);
        setContentView(R.layout.activity_home);
        mteTextViewBook = (TextView) findViewById(R.id.txt_book);
        mteTextViewBook.setOnClickListener(this);
        mTextAuthor = (TextView) findViewById(R.id.txt_author);
        mTextAuthor.setOnClickListener(this);
        mTextBlurb = (TextView) findViewById(R.id.txt_blurb);
        mTextBlurb.setOnClickListener(this);
        mTextInfo = (TextView) findViewById(R.id.txt_info);
        mTextInfo.setOnClickListener(this);
        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                R.anim.slide_in_right, 0, 0, R.anim.slide_out_right
        ).replace(R.id.container, new FragmentCoverPage()).commit();
        mteTextViewBook.setSelected(true);
        mTextAuthor.setSelected(false);
        mTextBlurb.setSelected(false);
        mTextInfo.setSelected(false);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_book:
                if (!mteTextViewBook.isSelected() || BooksSharedPreference.getInstance(this).islogin(false)) {
                    BooksSharedPreference.getInstance(this).setLogin(false);
                    mteTextViewBook.setSelected(true);
                    mTextAuthor.setSelected(false);
                    mTextBlurb.setSelected(false);
                    mTextInfo.setSelected(false);
                    FragmentTableContent myFragment = (FragmentTableContent) getSupportFragmentManager()
                            .findFragmentByTag("TemplateFragment");
                    if (myFragment != null && myFragment.isVisible()) {
                        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_current_to_right);
                        animation.setDuration(300);
                        myFragment.getView().startAnimation(animation);
                        getSupportFragmentManager().beginTransaction().setCustomAnimations(
                                R.anim.slide_in_left, 0, 0, R.anim.slide_out_left
                        ).remove(myFragment);
                    }
                    getSupportFragmentManager().beginTransaction().setCustomAnimations(
                            R.anim.slide_in_left, 0, 0, R.anim.slide_out_left
                    ).replace(R.id.container, new FragmentCoverPage()).commit();
                    mteTextViewBook.setTextColor(getResources().getColor(R.color.white));
                    mTextAuthor.setTextColor(getUnselectedTxtColor());
                    mTextBlurb.setTextColor(getUnselectedTxtColor());
                     mTextInfo.setTextColor(getUnselectedTxtColor());
                    mTextInfo.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableinfo(),//top
                            0, //right
                            0);//bottom
                    mTextAuthor.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableAuthor(), //top
                            0, //right
                            0);//bottom
                    mteTextViewBook.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            R.mipmap.selected_book, //top
                            0, //right
                            0);//bottom
                    mTextBlurb.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableBlurb(), //top
                            0, //right
                            0);//bottom
                }
                break;
            case R.id.txt_author:
                if (!mTextAuthor.isSelected()) {
                    mteTextViewBook.setSelected(false);
                    mTextAuthor.setSelected(true);
                    mTextBlurb.setSelected(false);
                    mTextInfo.setSelected(false);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentAuthor()).commit();
                    mTextAuthor.setTextColor(getResources().getColor(R.color.white));
                    mteTextViewBook.setTextColor(getUnselectedTxtColor());
                    mTextBlurb.setTextColor(getUnselectedTxtColor());
                    mTextInfo.setTextColor(getUnselectedTxtColor());
                    mTextInfo.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableinfo(),//top
                            0, //right
                            0);//bottom
                    mTextAuthor.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            R.mipmap.selected_author, //top
                            0, //right
                            0);//bottom
                    mteTextViewBook.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableBook(), //top
                            0, //right
                            0);//bottom
                    mTextBlurb.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableBlurb(), //top
                            0, //right
                            0);//bottom
                }
                break;
            case R.id.txt_blurb:
                if (!mTextBlurb.isSelected()) {
                    mteTextViewBook.setSelected(false);
                    mTextAuthor.setSelected(false);
                    mTextBlurb.setSelected(true);
                    mTextInfo.setSelected(false);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentBlurb()).commit();
                     mteTextViewBook.setTextColor(getUnselectedTxtColor());
                     mTextAuthor.setTextColor(getUnselectedTxtColor());
                    mTextBlurb.setTextColor(getResources().getColor(R.color.white));
                    mTextInfo.setTextColor(getUnselectedTxtColor());
                    mTextInfo.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableinfo(),//top
                            0, //right
                            0);//bottom
                    mTextAuthor.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableAuthor(), //top
                            0, //right
                            0);//bottom
                    mteTextViewBook.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableBook(), //top
                            0, //right
                            0);//bottom
                    mTextBlurb.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            R.mipmap.selected_blurb, //top
                            0, //right
                            0);//bottom
                }
                break;

            case R.id.txt_info:
                if (!mTextInfo.isSelected()) {
                    mteTextViewBook.setSelected(false);
                    mTextAuthor.setSelected(false);
                    mTextBlurb.setSelected(false);
                    mTextInfo.setSelected(true);
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, new FragmentInfo()).commit();
                    mteTextViewBook.setTextColor(getUnselectedTxtColor());
                    mTextAuthor.setTextColor(getUnselectedTxtColor());
                    mTextBlurb.setTextColor(getUnselectedTxtColor());
                    mTextInfo.setTextColor(getResources().getColor(R.color.white));
                    mTextInfo.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            R.mipmap.selected_abotus,//top
                            0, //right
                            0);//bottom
                    mTextAuthor.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableAuthor(), //top
                            0, //right
                            0);//bottom
                    mteTextViewBook.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableBook(), //top
                            0, //right
                            0);//bottom
                    mTextBlurb.setCompoundDrawablesWithIntrinsicBounds(
                            0, //left
                            getUnselectedDrawableBlurb(), //top
                            0, //right
                            0);//bottom
                }
                break;

        }

    }

    private int getUnselectedTxtColor() {
        switch (BooksSharedPreference.getInstance(this).getTheme("GREEN")) {
            case ConstantUtill.THEME_BLUE:
                return getResources().getColor(R.color.unselected_txt_color_blue);
            case ConstantUtill.THEME_GREEN:
                return getResources().getColor(R.color.unselected_txt_color_green);
            case ConstantUtill.THEME_DARK_GREEN:
                return getResources().getColor(R.color.unselected_txt_color_dark_green);
            case ConstantUtill.THEME_SEA_GREEN:
                return getResources().getColor(R.color.unselected_txt_color_sea_green);
        }
        return getResources().getColor(R.color.unselected_txt_color_green);

    }
    private int getUnselectedDrawableAuthor() {
        switch (BooksSharedPreference.getInstance(this).getTheme("GREEN")) {
            case ConstantUtill.THEME_BLUE:
                return R.mipmap.blue_ic_unselected_author;
            case ConstantUtill.THEME_GREEN:
                return R.mipmap.green_ic_unselected_author;
            case ConstantUtill.THEME_DARK_GREEN:
                return R.mipmap.dark_green_ic_unselected_author;
            case ConstantUtill.THEME_SEA_GREEN:
                return R.mipmap.see_green_ic_unselected_author;
        }
        return R.mipmap.green_ic_unselected_author;

    }

    private int getUnselectedDrawableinfo() {
        switch (BooksSharedPreference.getInstance(this).getTheme("GREEN")) {
            case ConstantUtill.THEME_BLUE:
                return R.mipmap.blue_ic_unselected_abotus;
            case ConstantUtill.THEME_GREEN:
                return R.mipmap.green_ic_unselected_abotus;
            case ConstantUtill.THEME_DARK_GREEN:
                return R.mipmap.dark_green_ic_unselected_abotus;
            case ConstantUtill.THEME_SEA_GREEN:
                return R.mipmap.see_green_ic_unselected_abotus;
        }
        return R.mipmap.green_ic_unselected_abotus;

    }
    private int getUnselectedDrawableBook() {
        switch (BooksSharedPreference.getInstance(this).getTheme("GREEN")) {
            case ConstantUtill.THEME_BLUE:
                return R.mipmap.blue_ic_unselected_book;
            case ConstantUtill.THEME_GREEN:
                return R.mipmap.green_ic_unselected_book;
            case ConstantUtill.THEME_DARK_GREEN:
                return R.mipmap.dark_green_ic_unselected_book;
            case ConstantUtill.THEME_SEA_GREEN:
                return R.mipmap.see_green_ic_unselected_book;
        }
        return R.mipmap.green_ic_unselected_book;

    }
    private int getUnselectedDrawableBlurb() {
        switch (BooksSharedPreference.getInstance(this).getTheme("GREEN")) {
            case ConstantUtill.THEME_BLUE:
                return R.mipmap.blue_ic_unselected_blurb;
            case ConstantUtill.THEME_GREEN:
                return R.mipmap.green_ic_unselected_blurb;
            case ConstantUtill.THEME_DARK_GREEN:
                return R.mipmap.dark_green_ic_unselected_blurb;
            case ConstantUtill.THEME_SEA_GREEN:
                return R.mipmap.see_green_ic_unselected_blurb;
        }
        return R.mipmap.green_ic_unselected_blurb;

    }




    @Override
    public void onBackPressed() {
        finish();
    }
}
